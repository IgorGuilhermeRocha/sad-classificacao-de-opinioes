from typing import List
from unidecode import unidecode
import re
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import accuracy_score

STOP_WORDS = ['de', 'a', 'o', 'que', 'e', 'do', 'da', 'em', 'um', 'para', 
'com']

def read_file(path_to_file : str) -> List[str]:
    with open(path_to_file) as file :
        opinions = file.read().replace('\n','').split('\\\\')
        remove_blank_lines(opinions)
        opinions = normalize_data(opinions)
        return opinions

def remove_blank_lines(opinions : List[str]) -> None:
    for opinion in opinions: 
        if opinion == '' : opinions.remove(opinion)

def normalize_data(opinions : List[str]) -> List[str]:
    opinions = transform_to_lower_case(opinions)
    opinions = remove_accents(opinions)
    opinions = remove_non_letters(opinions)
    opinions = remove_stop_words(opinions)
    return opinions


def transform_to_lower_case(opinions : List[str]) -> List[str]:
    lower_opinions = [opinion.lower() for opinion in opinions]
    return lower_opinions 

def remove_accents(opinions : List[str]) -> List[str]:
    opinions_without_accent = [unidecode(opinion) for opinion in opinions]
    return opinions_without_accent

def remove_non_letters(opinions : List[str]) -> List[str]:
    opinions_with_only_letters = [re.sub(r'[^a-zA-Z\s]', '', opinion) for opinion in opinions]
    return opinions_with_only_letters

def remove_stop_words(opinions : List[str]) -> List[str]:
    filtered_opinions = []
    for phrase in opinions:
        words = phrase.split()
        filtered_words = [word for word in words if word.lower() not in STOP_WORDS]
        filtered_opinions.append(" ".join(filtered_words))
    return filtered_opinions


def print_data(opinions : List[str]) -> None:
    for opinion in opinions:
        print(opinion)
        print()

def main ():
    
    positive_opinions_for_training = read_file('data/training/positive_opinions_for_training.txt')
    negative_opinions_for_training = read_file('data/training/negative_opinions_for_training.txt')
    complete_training_data = positive_opinions_for_training + negative_opinions_for_training
    training_labels = ['positive'] * len(positive_opinions_for_training) + ['negative'] * len(negative_opinions_for_training)

    positive_opinions_for_test = read_file('data/test/positive_opinions_for_test.txt')
    negative_opinions_for_test = read_file('data/test/negative_opinions_for_test.txt')
    complete_test_data = positive_opinions_for_test + negative_opinions_for_test
    test_labels = ['positive'] * len(positive_opinions_for_test) + ['negative'] * len(negative_opinions_for_test)

    vectorizer = CountVectorizer()
    vectors_for_training = vectorizer.fit_transform(complete_training_data)
    vectors_for_test = vectorizer.transform(complete_test_data)
    print(vectors_for_training)

    clf = MultinomialNB()
    clf.fit(vectors_for_training, training_labels)
    predictions = clf.predict(vectors_for_test)

    print('Resultados: ')

    for iterator in range(len(test_labels)):
        expected = test_labels[iterator]
        got = predictions[iterator]

        if expected != got : 
            print('Erro!')
            print()
            if expected == 'positive':
                print(positive_opinions_for_test[iterator])
            else :
                print(negative_opinions_for_test[iterator - 10])
        else: print('Acerto!')
           
        print(f'Esperado: {test_labels[iterator]}, atingido: {predictions[iterator]}')
        print()
    
    print('Precisão: ', accuracy_score(test_labels, predictions))


main()
